# Instalando o Oh My ZSH no terminal

Primeiro instale o pacote `zsh`
```bash
sudo apt install zsh
```

Para verificar se o `zsh` foi instalado rode o seguinte comando:
```bash
zsh --version
```

A saída do meu console foi essa:
```bash
zsh 5.8 (x86_64-ubuntu-linux-gnu)
```

A sua saída pode ser um pouco diferente, mas o importante é ter uma versão instalada do zsh.

O proximo passo é necessário que você tenha instalado o `curl` ou `wget` para instalar o `oh my zsh`. De novo para verificar se já tem essas ferramentas instaladas rode os comandos abaixo e verifique a saída.
```bash
curl --version
```

```bash
wget --version
```


Caso não tenha, instale atravéz dos comandos
```bash
sudo apt install curl
```
```bash
sudo apt install wget
```

Depois disso copie e cole um dos comandos abaixo no seu terminal.
com `curl`
```bash
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```
com `wget`
```bash
sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

Durante a instalação você vai ser perguntado se quer torna o `oh my zsh` como shell padrão, basta apenas digitar `Y`. Você verá que seu terminal já vai mudar o visual.

## Alterando o tema do terminal

Caso queira alterar o tema do terminal, você pode alterar o valor da propriedade `ZSH_THEME` do arquivo `.zshrc` que fica na sua pasta de usuário. Ex.:
```zsh
ZSH_THEME="robbyrussell"
```

O tema `robbyrussell` é o que veio por padrão. Você pode encontrar informações sobre os temas no link [https://github.com/ohmyzsh/ohmyzsh/wiki/Themes](https://github.com/ohmyzsh/ohmyzsh/wiki/Themes) e substituir o nome `robbyrussell` pelo tema que você escolher.

* **Nota: o tema escolhido por vocẽ tem que existir dentro da pasta `.oh-my-zsh\themes` que fica na pasta do seu usuário.** *

Caso o arquivo não exista dentro da pasta, você pode adicoina-lo a pasta desde que você tenha acesso ao mesmo.

## Adicoinando recursos

Vamos instalar alguns plugins para deixar nosso terminal com alguns recursos bem legais. Vão ser 3 plugins, um de sugestão, auto-complete e highlighting.

Para instalar esses plugins primeiro precisamos instalar o `zinit`, que um gerenciador de plugins do `zsh`. Vamos usar o `curl` para instalar.
```bash
sh -c "$(curl -fsSL https://raw.githubusercontent.com/zdharma/zinit/master/doc/install.sh)"
```

Durante a instalação é só apertar `Enter`.

Código a ser copiado
```
zinit light zdharma/fast-syntax-highlighting
zinit light zsh-users/zsh-autosuggestions
zinit light zsh-users/zsh-completions
```


Com o `zinit` instalado vamos copiar as 3 linhas acima e colar no arquivo `.zshrc` depois da seguinte linha `### End of Zinit's installer chunk`. Como no exemplo abaixo.

```bash
### End of Zinit's installer chunk

zinit light zdharma/fast-syntax-highlighting
zinit light zsh-users/zsh-autosuggestions
zinit light zsh-users/zsh-completions

```

Com o terminal ainda aberto rode o comando abaixo para que o terminal reconheça as mudanças no arquivo `.zshrc`. A instalação vai ser feita e os plugins já vão estar funcoinando.

```bash
source ~/.zshrc

```






