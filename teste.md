
  
## Como usar zeedhi novo dentro de um iframe

### No Zeedhi antigo:

#####  Arquivo json
- Usar o template de iframe `widget/iframe.html`
- Definir como string vazia a propriedade `sourceUrl` da widget
- Criar evento `containerAfterinit`

````
{

	"name":  "clientsByOperator",

	"label":  "Clientes Por Operador",

	"showFooter":  true,

	"showHeader":  true,

	"showMenu":  true,

	"showBack":  true,

	"popup":  "component/popup.html",

	"template":  "container/window.html",

	"footer":  "component/footer.html",

	"menuTemplate":  "component/menu.html",

	"events":  [

		{

		"id":  "19180107471736243305634",

		"name":  "ContainerAfterinit",

		"code":  "ClientsByOperatorController.clientByOperatorContainerAfterinit()"

		}

	],

	"widgets":  [

		{

			"id":  "19180107472489502003632",

			"name":  "clientsByOperator",

			"isVisible":  true,

			"label":  "Clientes Por Operador",

			"template":  "widget/iframe.html",

			"sourceUrl":  "",

			"dataSource":  {

				"rest":  false,

				"localStorage":  false,

				"lazyLoad":  true,

				"data":  []

			},

			"events":  [],

			"actions":  [],

			"fields":  [],

			"widgets":  []

		}

	],

	"id":  "9517503591578592376631",

	"parentMenuId":  "9088430362653897906635",

	"parentMenuName":  "clientsByOperators",

	"parentMenuLabel":  "Clientespor Operador"

}
````

#####  Controller Javascript
- No envento do container obeter token e hash do login no localStorage.
- Definir url para setar na propriedade `sourceUrl` da widget.
	- A url aponta para a página do Zeedhi Next que deseja ser mostrada
	- Todos os parâmetros que precisam ser passados para dentro do iFrame (Zeedhi "velho" para Zeedhi Next) devem ser passados como parâmetros da url, como no exemplo abaixo:
	- `"sourceUrl": "http://localhost:8080/suppliers?header=0&menu=0&token=123"`

````
function  ClientsByOperatorController(ScreenService, templateManager){

	var getWidgetClientsByOperator =  function(){
		return templateManager.container.getWidget('clientsByOperator');
	};

	this.clientByOperatorContainerAfterinit =  function  ()  {
		var token = localStorage.getItem('LOGIN_TOKEN');
		var hash = localStorage.getItem('LOGIN_HASH')  ||  '0';
		var url =  OMConfig.omNextUrl +  "/clientes-por-operador?header=0&menu=0&token="  + token +  "&hash="  + hash;
		getWidgetClientsByOperator().sourceUrl = url;
	};
}

Configuration(function(ContextRegister){
	ContextRegister.register('ClientsByOperatorController',  ClientsByOperatorController);
});
````

Obs:
\* A proŕiedde `sourceUrl` é setada via javascript pelo fato de ter que pegar as informações de login dinamicamente.

#####  Arquivo CSS
- Definir as propriedades **`height`** e **`width`** em 100% para o `iframe`
````
iframe  {
	width:  100%;
	height:  100%;
}
````

### No Zeedhi novo:
#### App.vue

- Colocar `v-if` no **menu, header** e **footer** para que possam ser controlados a partir de parâmetros vindo da aplicação que está chamando:
	````
	<component v-if="showMenu" :is="menu.component" ref="menuInstance" v-bind="menu"></component>
	<component v-if="showHeader" :is="header.component" ref="headerInstance" v-bind="header"></component>
	````
- Na tag `<script>`do arquivo `App.vue`:
	- Importar a store `import Store from './store';` 
	- Adicionar métodos **showMenu** e **showHeader**:

````
public get showMenu(): boolean {
	return  Store.state.appShowMenu;
}

public get showHeader(): boolean {
	return  Store.state.appShowHeader;
}
````	

[Link App.vue](https://gitlab.teknisa.com/-/ide/project/teknisa/organizations-management-next/tree/master/-/frontend/src/App.vue/)

#### router.ts
- Adicionar um método **router.beforeEach** para tratar os parâmetros recebidos:
	````
	router.beforeEach((to,  from, next)  =>  {
		if  (window.parent !== window && to.name !==  'token-error')  {
			if  (to.query.token)  {
				localStorage.setItem('APP_LOGIN_TOKEN',  JSON.stringify(to.query.token));
				if  (to.query.hash !==  '0')  {
					localStorage.setItem('APP_LOGIN_HASH',  JSON.stringify(to.query.hash));
				}
				Store.commit('setAppLoginToken', to.query.token);
				Store.commit('setAppDisplayOptions',  { menu: to.query.menu, header: to.query.header });
			}
			
			if  (!Store.state.appLoginToken)  {
				// if there is no token... redirect to another page
				Store.commit('setAppDisplayOptions',  { menu:  '0', header:  '0'  });
				next('/token-error');
				return;
			}
			
			// remove parameters from requested page
			delete to.query.menu;
			delete to.query.header;
			delete to.query.token;
		}
		next();
	});
	````

[Link router.ts](https://gitlab.teknisa.com/-/ide/project/teknisa/organizations-management-next/tree/master/-/frontend/src/router/router.ts/)

#### store.ts
- Adicionar métodos para salvar parâmetros:

	````
	export  default  new  Vuex.Store({
		state: {
			appToken: '',
			appLoginHash:  '',
			appShowMenu: true,
			appShowHeader: true,
		},
		mutations: {
			setAppLoginToken: (state, token: string) => {
				state.appToken = token;
			},
			setAppLoginHash:  (state, hash:  string)  =>  {
				state.appLoginHash = hash;
			},
			setAppDisplayOptions: (state, options: any) => {
				state.appShowMenu = options.menu !== '0';
				state.appShowHeader = options.header !== '0';
			},
		},
		actions: {},
	});
	````
[Link index.ts (equivalente ao store.js)](https://gitlab.teknisa.com/-/ide/project/teknisa/organizations-management-next/tree/master/-/frontend/src/store/index.ts/)

Snippets feito pelo Zéd que demontra o mesmo processo explicado acima:
[http://code.zeedhi.com/snippets/109](http://code.zeedhi.com/snippets/109)
